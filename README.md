
Websocket Microservice
=========================

A tiny microservice for quick + easy websockets on the backend.

To open a websocket connection:

```
GET /?session_id=cc49c866281c4a5571b0112e6df59e16 HTTP/1.1
Upgrade: websocket
Host: localhost:8080
Origin: http://localhost:8080
Sec-WebSocket-Key: I7YyUoNKlNQm9pgXC1Egzg==
Sec-WebSocket-Version: 13
Connection: Upgrade
```

NOTE: session id **MUST** be a hexstring of length 32 chars.

To send a message to this client:

```
$ curl -v -XPOST -d 'hello world' 'http://localhost:8080?session_id=cc49c866281c4a5571b0112e6df59e16'
Note: Unnecessary use of -X or --request, POST is already inferred.
*   Trying 127.0.0.1:8080...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> POST /?session_id=cc49c866281c4a5571b0112e6df59e16 HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.87.0
> Accept: */*
> Content-Length: 11
> Content-Type: text/plain
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Content-Type: application/json
< Date: Sun, 12 Feb 2023 19:10:27 GMT
< Content-Length: 14
<
{"Sent":true}
```

All messages are sent as websocket string type.
