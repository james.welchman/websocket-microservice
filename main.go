package main

import (
	"crypto/sha1"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/flickpp/websocket/config"
	"github.com/flickpp/websocket/websocket"
)

var (
	WrongMethod              = errors.New("method not supported")
	MissingSessionId         = errors.New("missing session id query")
	DuplicateSessionIdQuery  = errors.New("duplicate session_id= query param")
	InvalidSessionId         = errors.New("invalid session_id")
	BadVersionHeader         = errors.New("invalid websocket version header")
	BadConnectionHeader      = errors.New("invalid connection header")
	DuplicateSecWebsocketKey = errors.New("duplicate Sec-Websocket-Key header")
	NotWebsocketRequest      = errors.New("not a valid websocket request")
)

const WebsocketMagicSuffix = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"

type ResponseError struct {
	err  error
	code int
}

type SendMsgResponse struct {
	Sent bool `json:sent`
}

func main() {
	config := config.NewConfig()

	server := &http.Server{
		Addr: config.BindAddr,
		Handler: &Handler{
			lock:    sync.Mutex{},
			clients: make(map[string]chan []byte, 128),
		},
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	log.Fatal(server.ListenAndServe())
}

type Handler struct {
	lock    sync.Mutex
	clients map[string]chan []byte
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		// Websocket
		h.websocketRequest(w, r)
	} else if r.Method == "POST" {
		// Send payload
		h.sendMsgRequest(w, r)
	} else {
		http.Error(w, WrongMethod.Error(), 400)
	}
}

func (h *Handler) websocketRequest(w http.ResponseWriter, r *http.Request) {
	// We must have:
	//   1. query session_id=${session_id} where session_id is a hexstring of 32 chars
	//   2. Header - Upgrade: Websocket
	//   3. Header - Connection: Upgrade
	//   4. Sec-Websocket-Key: ${key}
	//   5. Header - Sec-Websocket-Version: 13

	sessionId, rErr := getSessionId(r.URL)
	if rErr != nil {
		http.Error(w, rErr.err.Error(), rErr.code)
		return
	}

	// Lock client map
	h.lock.Lock()

	if _, ok := h.clients[sessionId]; ok {
		// This session id is already connected
		http.Error(w, "sessionId already has websocket connection", 400)
		return
	}

	secWebsocketKey, rErr := readWebsocketHeaders(r.Header)
	if rErr != nil {
		http.Error(w, rErr.err.Error(), rErr.code)
		return
	}

	// Compute Sec-Websocket-Accept
	secWebsocketAccept := computeSecWebsocketAccept(secWebsocketKey)

	w.Header().Add("Upgrade", "Websocket")
	w.Header().Add("Connection", "Upgrade")
	w.Header().Add("Sec-Websocket-Accept", secWebsocketAccept)
	w.WriteHeader(101)

	ch := make(chan []byte, 8)

	// Place channel into map for sendMsg to find it
	h.clients[sessionId] = ch
	h.lock.Unlock()

	// Run the websocket
	websocket.Run(r.Body, w, ch)

	// Okay we're done - remove this sessionId client from map
	h.lock.Lock()
	delete(h.clients, sessionId)
	h.lock.Unlock()
}

func (h *Handler) sendMsgRequest(w http.ResponseWriter, r *http.Request) {
	sessionId, rErr := getSessionId(r.URL)
	if rErr != nil {
		http.Error(w, rErr.err.Error(), rErr.code)
	}

	h.lock.Lock()
	defer h.lock.Unlock()
	ch, ok := h.clients[sessionId]
	encoder := json.NewEncoder(w)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)

	// 200 Ok is implicity written
	if !ok {
		encoder.Encode(&SendMsgResponse{Sent: false})
	} else {

		// Read entire body into buffer
		body := make([]byte, r.ContentLength)
		var bytesRead int64 = 0
		for bytesRead < r.ContentLength {
			n, err := r.Body.Read(body[bytesRead:])

			if err != io.EOF && err != nil {
				// How to handle an i/o error?
				// For now just exit
				return
			}
			bytesRead += int64(n)

			if err == io.EOF {
				break
			}
		}

		encoder.Encode(&SendMsgResponse{Sent: true})

		// Send body to websocket
		ch <- body

	}
}

func getSessionId(url *url.URL) (string, *ResponseError) {
	var sessionId string = ""

	for key, val := range url.Query() {
		if strings.ToLower(key) == "session_id" {
			if len(val) != 1 {
				return "", respErr(DuplicateSessionIdQuery, 400)
			}
			sessionId = val[0]
			break
		}

		// Ignore all other query params
	}

	if sessionId == "" {
		return "", respErr(MissingSessionId, 400)
	}

	sessionId = strings.ToLower(sessionId)
	raw, err := hex.DecodeString(sessionId)
	if err != nil {
		return "", respErr(err, 400)
	}

	if len(raw) != 16 {
		return "", respErr(InvalidSessionId, 400)
	}

	return sessionId, nil
}

func respErr(err error, code int) *ResponseError {
	return &ResponseError{
		err:  err,
		code: code,
	}
}

func readWebsocketHeaders(headers http.Header) (string, *ResponseError) {
	upgrade := false
	connection := false
	version := 0
	secWebsocketKey := ""

	for key, heads := range headers {
		key := strings.ToLower(key)

		switch key {
		case "upgrade":
			if len(heads) != 1 || strings.ToLower(heads[0]) != "websocket" {
				fmt.Println("ici1")
				return "", respErr(BadVersionHeader, 400)
			}
			upgrade = true
		case "connection":
			if len(heads) != 1 || strings.ToLower(heads[0]) != "upgrade" {
				fmt.Println("ici2")
				return "", respErr(BadConnectionHeader, 400)
			}

			connection = true
		case "sec-websocket-version":
			if len(heads) != 1 {
				fmt.Println("ici3")
				return "", respErr(BadVersionHeader, 400)
			}

			i, err := strconv.Atoi(heads[0])
			if err != nil {
				fmt.Println("ici4")
				return "", respErr(BadVersionHeader, 400)
			}

			if i != 13 {
				fmt.Println("ici5")
				return "", respErr(BadVersionHeader, 400)
			}

			version = 13
		case "sec-websocket-key":
			if len(heads) != 1 {
				fmt.Println("ici6")
				return "", respErr(DuplicateSecWebsocketKey, 400)
			}

			secWebsocketKey = heads[0]
		}
	}

	if !upgrade || !connection || version != 13 || secWebsocketKey == "" {
		return "", respErr(NotWebsocketRequest, 400)
	}

	return secWebsocketKey, nil
}

func computeSecWebsocketAccept(key string) string {
	digest := sha1.Sum([]byte(key + WebsocketMagicSuffix))
	return base64.StdEncoding.EncodeToString(digest[:])
}
