package websocket

// TODO: Data framing (ping + strings)
// TODO: Read pings/pongs from client

import (
	"io"
	"math/rand"
	"time"
)

var fallBackPingValue = [8]byte{0xec, 0xe2, 0x38, 0x66, 0xef, 0x07, 0x4f, 0xa1}

func Run(r io.Reader, w io.Writer, ch <-chan []byte) {
	pingTimer := time.NewTimer(30 * time.Second)
	rng := rand.New(rand.NewSource(0))

	for {
		select {
		case body := <-ch:
			// Send message to client
			msg := buildStringMsg(body)

			bytesWritten := 0
			for bytesWritten < len(msg) {
				n, err := w.Write(msg[bytesWritten:])
				if err != nil {
					return
				}

				bytesWritten += n
			}
		case <-pingTimer.C:
			// Send a ping to client

			val := [8]byte{}
			if _, err := rng.Read(val[:]); err != nil {
				copy(val[:], fallBackPingValue[:])
			}
			if err := sendPing(w, val); err != nil {
				// Couldn't write to conn - abandon
				return
			}
		}
	}
}

// Write ping
func sendPing(w io.Writer, val [8]byte) error {
	// TODO
	return nil
}

func buildStringMsg(payload []byte) []byte {
	// TODO: Build string message for websocket
	// https://www.rfc-editor.org/rfc/rfc6455#section-5
	return payload
}
