package config

import (
	"os"
)

type Config struct {
	BindAddr string
}

func NewConfig() *Config {
	cfg := &Config{
		BindAddr: "0.0.0.0:8080",
	}

	bindAddr, ok := os.LookupEnv("WEBSOCKET_BIND_ADDR")
	if !ok {
		bindAddr = "0.0.0.0:8080"
	}
	cfg.BindAddr = bindAddr

	return cfg
}
